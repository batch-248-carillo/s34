//express package was imported as express
const express = require("express");

//invoked express package to create a server/api and save it in a variable which we can refert to later to create routes
const app = express();

//variable for port assignment
const port = 4000;

//express.json() is a method from express that allow us to handle the stream of data from our client and receive the data and automatically parse the incoming JSON from our request

//app.use() is a method used to run another function or method for our expressjs api
//it is used to run middlewares

app.use(express.json())

//by applying the option of "extended:true" this allows us to receive information in other data types such as an object whcih we will use throughout our application
app.use(express.urlencoded({extended:true}))

//CREATE A ROUTE IN EXPRESS
	//express has methods corresponding to each HTTP Method


app.get("/",(req,res)=>{

	//res.send uses the express JS Module's method instead to send a response back to the client
	res.send("Hello from our default ExpressJs GET route!")
})

app.post("/",(req,res)=>{

	//res.send("Hello from our default ExpressJs POST route!")
	res.send(`Hello ${req.body.firstName} ${req.body.lastName}`)
})

//MA1
	//Refactor the post route to receive data from the req.body
	//Send a message that has the data received
//MA2
	//Create a route for a put method request on the "/" endpoint
	//Send a message as a response: "Hello from the ExpressJS PUT method route!"

	//Create a route for a delete method request on the "/" endpoint
	//Send a message as a response: "Hello from the ExpressJS DELETE method route!"

			//{
			//    "firstName": "Cardo",
			//    "lastName": "Dalisay"
			//}

app.put('/',(req,res)=>{

	res.send("Hello from a put method route!")

})

app.delete('/',(req,res)=>{

	res.send("Hello from a delete method route!")

})

//Add a Mock Database

let users = [

		{
			username: "cardo_dalisay",
			password:"quiapo"	
		},
		{
			username: "mommy_d",
			password:"nardadarna"	
		},
		{
			username: "kagawad_godbless",
			password:"dingangbato"	
		}
	]


app.post('/signup',(req,res)=>{

	//request/req.body contains the body of the request or the input
	users.push(req.body)
	res.send(users)
})

//MA3

//Refactor the signup route
	//If contents of the "request body" with the property "username" and "password" is not empty, this will store the user object sent via Postman to the users array created above
	//This will send a response back to the client/Postman after the request has been processed

	//Else the username and password are not complete an error message will be sent back to the client/Postman

	app.post("/register", (req, res) => {

	    console.log(req.body);

	    // If contents of the "request body" with the property "username" and "password" is not empty
	    if(req.body.username !== '' && req.body.password !== ''){

	        // This will store the user object sent via Postman to the users array created above
	        users.push(req.body);

	        // This will send a response back to the client/Postman after the request has been processed
	        res.send(`User ${req.body.username} successfully registered!`);

	    // If the username and password are not complete an error message will be sent back to the client/Postman
	    } else {

	        res.send("Please input BOTH username and password.");

	    }

	})

	//this route expectes to receive a PUT request at the URI "/change-password"


	app.put("/change-password",(req,res)=>{

		//(1) creates a variable to store the message to be sent back to Postman
		let message;
		//Creates a for lopp that will loop through the elements of the "users" array
		for(let i=0; i<users.length; i++){

			if (req.body.username == users[i].username){

				users[i].password = req.body.password;

				message = `User ${req.body.username}'s password has been updated!`;

				break;
			}else{

				message = "User does not exist!"

			}
		}
		res.send(message);
	})

	//Activity
	//1. Create a GET route that will access the "/home" route that will print out a simple message.

	
	//2. Create a GET route that will access the "/users" route that will retrieve all the users in the mock database.

	//3. Create a DELETE route that will access the "/delete-user" route to remove a user from the mock database.



// ACTIVITY


//1. 

	app.get("/home",(req,res)=>{

	res.send("Welcome to home page!")
})
	
//2.

	app.get("/users",(req,res)=>{

	res.send(users)
})

//3.


	app.delete('/user-delete', (req, res) => {

			let message;
			let userIndex=-1;

			for(let i=0; i<users.length; i++){

				if (req.body.username == users[i].username){
					userIndex=i;}

				else if (userIndex == -1){
					message = `User "${req.body.username}" does not found!`;
					
				}else {
					users.splice(userIndex, 1);
					message = `User "${req.body.username}" has been successfully deleted!`;
					res.send(message);
				}
			}
		});
				


	//tells our server to listen to the port
	app.listen(port,()=> console.log(`Server running at port ${port}`))

